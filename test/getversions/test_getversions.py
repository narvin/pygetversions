"""Unit test module for the getversions module."""

from subprocess import CalledProcessError, CompletedProcess, DEVNULL
import sys
from typing import Optional, Tuple

import pytest
from pytest_mock.plugin import MockerFixture

from getversions import (
    get_avail_versions,
    get_installed_version,
    print_versions,
    is_installed_in_avail_versions,
)
from getversions.getversions import (
    _AvailVersionStrategy,
    _get_pip_version,
    _index_versions_avail_versions_strategy,
    _double_equal_avail_versions_strategy,
    _legacy_resolver_avail_versions_strategy,
    _not_implemented_avail_versions_strategy,
    _get_avail_versions_strategy,
)

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterable
else:
    from typing import Iterable


class TestGetVersionsModule:
    """Test suite for the getversions module."""

    @staticmethod
    def test_get_pip_version(mocker: MockerFixture) -> None:
        """Test ``_get_pip_version`` returns the correct version of pip."""
        mock_check_output = mocker.patch(
            "getversions.getversions.check_output",
            return_value=" ".join(
                (
                    "pip 22.2.2",
                    "from /some/path/python3.10/site-packages/pip",
                    "(python 3.10)",
                )
            ),
        )
        assert _get_pip_version() == (22, 2, 2)
        mock_check_output.assert_called_once_with(
            f"{sys.executable} -m pip --version".split(" "), text=True
        )

    @staticmethod
    @pytest.mark.parametrize(
        "pip_version, exp_strategy",
        (
            ((22, 2, 2), _index_versions_avail_versions_strategy),
            ((21, 2, 0), _index_versions_avail_versions_strategy),
            ((21, 1, 0), _double_equal_avail_versions_strategy),
            ((21, 0, 99), _legacy_resolver_avail_versions_strategy),
            ((20, 3, 0), _legacy_resolver_avail_versions_strategy),
            ((20, 2, 99), _double_equal_avail_versions_strategy),
            ((9, 0, 0), _double_equal_avail_versions_strategy),
            ((8, 99, 99), _not_implemented_avail_versions_strategy),
        ),
    )
    def test_get_avail_versions_strategy(
        pip_version: Tuple[int, ...], exp_strategy: _AvailVersionStrategy
    ) -> None:
        """Test ``get_avail_versions_strategy`` returns the correct
        ``_AvailVersionStrategy``.
        """
        assert _get_avail_versions_strategy(pip_version) is exp_strategy

    @staticmethod
    @pytest.mark.parametrize(
        "pip_out, exp_versions",
        (
            (
                (
                    "mypkg (0.0.3)",
                    "Available versions: 0.0.3, 0.0.2, 0.0.1",
                ),
                ("0.0.3", "0.0.2", "0.0.1"),
            ),
            (
                (
                    "mypkg (0.0.3)",
                    "Available versions:",
                ),
                (),
            ),
            (
                (
                    "mypkg (0.0.3)",
                    "Available versions",
                ),
                None,
            ),
            (
                ("mypkg (0.0.3)",),
                None,
            ),
        ),
    )
    def test_get_avail_version(
        pip_out: Iterable[str],
        exp_versions: Optional[Iterable[str]],
        mocker: MockerFixture,
    ) -> None:
        """Test ``get_avail_version`` returns the correct versions, or ``None``."""
        mock_check_output = mocker.patch(
            "getversions.getversions.check_output",
            return_value=" ".join(
                (
                    "pip 22.2.2",
                    "from /some/path/python3.10/site-packages/pip",
                    "(python 3.10)",
                )
            ),
        )
        mock_run = mocker.patch(
            "getversions.getversions.run",
            return_value=CompletedProcess(
                args="", returncode=0, stdout="\n".join(pip_out)
            ),
        )
        assert get_avail_versions("mypkg") == exp_versions
        assert mock_check_output.called_once_with(
            mocker.call(f"{sys.executable} -m pip --version".split(" "), text=True)
        )
        assert mock_run.called_once_with(
            mocker.call(
                f"{sys.executable} -m pip index versions mypkg".split(" "),
                capture_output=True,
                check=True,
                text=True,
            ),
        )

    @staticmethod
    def test_get_avail_versions_raises_not_found(mocker: MockerFixture) -> None:
        """Test ``get_avail_versions`` returns ``None`` when a ``CalledProcessError``
        is raised for a package that isn't found.
        """
        mock_run = mocker.patch(
            "getversions.getversions.run",
            side_effect=CalledProcessError(
                1, "", "ERROR: No matching distribution found for mypkg\n"
            ),
        )
        assert get_avail_versions("mypkg") is None
        mock_run.assert_called_once_with(
            f"{sys.executable} -m pip index versions mypkg".split(" "),
            capture_output=True,
            check=True,
            text=True,
        )

    @staticmethod
    def test_get_avail_versions_raises(mocker: MockerFixture) -> None:
        """Test ``get_avail_versions`` reraises a ``CalledProcessError`` when the
        exception is raised for some reason other than the package isn't found.
        """
        mocker.patch(
            "getversions.getversions.run",
            side_effect=CalledProcessError(2, "foo", "bar", "baz"),
        )
        with pytest.raises(CalledProcessError, match="foo") as excinfo:
            get_avail_versions("mypkg")
        assert excinfo.value.returncode == 2
        assert excinfo.value.stdout == "bar"
        assert excinfo.value.stderr == "baz"

    @staticmethod
    @pytest.mark.parametrize(
        "pip_out, exp_version",
        (
            (
                (
                    "Name: mypkg",
                    "Version: 0.0.1",
                    "Summary: This is my package.",
                ),
                "0.0.1",
            ),
            (
                (
                    "Name: mypkg",
                    "Version:",
                    "Summary: This is my package.",
                ),
                "",
            ),
            (
                (
                    "Name: mypkg",
                    "Version",
                    "Summary: This is my package.",
                ),
                None,
            ),
            (
                (
                    "Name: mypkg",
                    "Summary: This is my package.",
                ),
                None,
            ),
        ),
    )
    def test_get_installed_version(
        pip_out: Iterable[str], exp_version: Optional[str], mocker: MockerFixture
    ) -> None:
        """Test ``get_installed_version`` returns the correct version, or ``None``."""
        mock_run = mocker.patch(
            "getversions.getversions.run",
            return_value=CompletedProcess(
                args="", returncode=0, stdout="\n".join(pip_out)
            ),
        )
        assert get_installed_version("mypkg") == exp_version
        mock_run.assert_called_once_with(
            f"{sys.executable} -m pip show mypkg".split(" "),
            capture_output=True,
            check=True,
            text=True,
        )

    @staticmethod
    def test_get_installed_version_raises_not_found(mocker: MockerFixture) -> None:
        """Test ``get_installed_version`` returns ``None`` when a
        ``CalledProcessError`` is raised for a package that isn't found.
        """
        mock_run = mocker.patch(
            "getversions.getversions.run",
            side_effect=CalledProcessError(
                1, "", "WARNING: Package(s) not found: mypkg\n"
            ),
        )
        assert get_installed_version("mypkg") is None
        mock_run.assert_called_once_with(
            f"{sys.executable} -m pip show mypkg".split(" "),
            capture_output=True,
            check=True,
            text=True,
        )

    @staticmethod
    def test_get_installed_version_raises(mocker: MockerFixture) -> None:
        """Test ``get_installed_version`` reraises a ``CalledProcessError`` when
        the exception is raised for some reason other than the package isn't found.
        """
        mocker.patch(
            "getversions.getversions.run",
            side_effect=CalledProcessError(2, "foo", "bar", "baz"),
        )
        with pytest.raises(CalledProcessError, match="foo") as excinfo:
            get_installed_version("mypkg")
        assert excinfo.value.returncode == 2
        assert excinfo.value.stdout == "bar"
        assert excinfo.value.stderr == "baz"

    @staticmethod
    @pytest.mark.parametrize(
        "avail_versions, installed_version, exp_out",
        (
            (("0.0.3", "0.0.2", "0.0.1"), "0.0.3", ("*0.0.3", "0.0.2", "0.0.1")),
            (
                ("0.0.3", "0.0.2", "0.0.1"),
                "0.0.4",
                ("0.0.3", "0.0.2", "0.0.1", "+0.0.4"),
            ),
            ((), "0.0.3", ("+0.0.3",)),
            (None, "0.0.3", ("0.0.3",)),
            (("0.0.3", "0.0.2", "0.0.1"), "", ("0.0.3", "0.0.2", "0.0.1")),
            (("0.0.3", "0.0.2", "0.0.1"), None, ("0.0.3", "0.0.2", "0.0.1")),
            ((), None, ()),
            ((), "", ()),
            (None, None, ()),
            (None, "", ()),
        ),
    )
    def test_print_versions(
        avail_versions: Optional[Iterable[str]],
        installed_version: Optional[str],
        exp_out: Iterable[str],
        capsys: pytest.CaptureFixture[str],
    ) -> None:
        """Test ``print_versions`` prints the correct output."""
        print_versions(avail_versions, installed_version)
        out, _ = capsys.readouterr()
        assert out == "\n".join((*exp_out, ""))

    @staticmethod
    @pytest.mark.parametrize(
        "avail_versions, installed_version, exp_res",
        (
            (("0.0.3", "0.0.2", "0.0.1"), "0.0.3", True),
            (("0.0.3", "0.0.2", "0.0.1"), "0.0.4", False),
            ((), "0.0.3", False),
            (None, "0.0.3", False),
            (("0.0.3", "0.0.2", "0.0.1"), "", False),
            (("0.0.3", "0.0.2", "0.0.1"), None, True),
            ((), None, True),
            ((), "", False),
            (None, None, True),
            (None, "", False),
        ),
    )
    def test_is_installed_in_avail_versions(
        avail_versions: Optional[Iterable[str]],
        installed_version: Optional[str],
        exp_res: bool,
    ) -> None:
        """Test ``is_installed_in_avail_versions`` returns the correct result."""
        assert (
            is_installed_in_avail_versions(avail_versions, installed_version) == exp_res
        )

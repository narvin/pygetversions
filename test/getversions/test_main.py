"""Unit test module for the main module."""

from subprocess import run
import sys
from typing import Optional, Tuple

import pytest
from pytest_mock.plugin import MockerFixture

from getversions.main import main

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterable, Sequence
else:
    from typing import Iterable, Sequence


class TestMainModule:
    """Test suite for the main module."""

    @staticmethod
    @pytest.mark.parametrize(
        "args, avail_versions, installed_version, exp_out, exp_res",
        (
            (
                ("foo",),
                ("0.0.3", "0.0.2", "0.0.1"),
                "0.0.3",
                ("*0.0.3", "0.0.2", "0.0.1"),
                0,
            ),
            (
                ("foo",),
                ("0.0.2", "0.0.1"),
                "0.0.3",
                ("0.0.2", "0.0.1", "+0.0.3"),
                0,
            ),
            (
                ("foo",),
                (),
                "0.0.3",
                ("+0.0.3",),
                0,
            ),
            (
                ("foo",),
                None,
                "0.0.3",
                ("0.0.3",),
                0,
            ),
            (
                ("foo",),
                ("0.0.3", "0.0.2", "0.0.1"),
                "",
                ("0.0.3", "0.0.2", "0.0.1"),
                0,
            ),
            (
                ("foo",),
                ("0.0.3", "0.0.2", "0.0.1"),
                None,
                ("0.0.3", "0.0.2", "0.0.1"),
                0,
            ),
            (
                ("foo", "-e"),
                ("0.0.3", "0.0.2", "0.0.1"),
                "0.0.3",
                ("*0.0.3", "0.0.2", "0.0.1"),
                1,
            ),
            (
                ("foo", "--exists-in-repo"),
                ("0.0.3", "0.0.2", "0.0.1"),
                "0.0.3",
                ("*0.0.3", "0.0.2", "0.0.1"),
                1,
            ),
            (
                ("foo", "-e"),
                ("0.0.2", "0.0.1"),
                "0.0.3",
                ("0.0.2", "0.0.1", "+0.0.3"),
                0,
            ),
        ),
    )
    def test_main_print_avail(
        args: Sequence[str],
        avail_versions: Optional[Iterable[str]],
        installed_version: Optional[str],
        exp_out: Iterable[str],
        exp_res: int,
        mocker: MockerFixture,
        capsys: pytest.CaptureFixture[str],
    ) -> None:
        """Test ``main`` produces the correct output when it should print available
        versions.
        """
        mock_get_avail_versions = mocker.patch(
            "getversions.main.get_avail_versions",
            return_value=avail_versions,
        )
        mock_get_installed_version = mocker.patch(
            "getversions.main.get_installed_version",
            return_value=installed_version,
        )
        res = main(args)
        out, _ = capsys.readouterr()
        assert out == "\n".join((*exp_out, ""))
        assert res == exp_res
        mock_get_avail_versions.assert_called_once_with(args[0])
        mock_get_installed_version.assert_called_once_with(args[0])

    @staticmethod
    @pytest.mark.parametrize(
        "args, avail_versions, installed_version, exp_out, exp_res",
        (
            (("foo", "-i"), None, "0.0.3", "0.0.3\n", 0),
            (("foo", "--installed-only"), None, "0.0.3", "0.0.3\n", 0),
            (("foo", "-ie"), ("0.0.3", "0.0.2", "0.0.1"), "0.0.3", "0.0.3\n", 0),
            (("foo", "-i"), None, "", "", 0),
            (("foo", "-i"), None, None, "", 0),
            (("foo", "-ie"), (), "", "", 0),
            (("foo", "-ie"), (), None, "", 1),
        ),
    )
    def test_main_no_print_avail(
        args: Sequence[str],
        avail_versions: Optional[Iterable[str]],
        installed_version: Optional[str],
        exp_out: str,
        exp_res: int,
        mocker: MockerFixture,
        capsys: pytest.CaptureFixture[str],
    ) -> None:
        """Test ``main`` produces the correct output when it should not print
        available versions.
        """
        mock_get_avail_versions = mocker.patch("getversions.main.get_avail_versions")
        mock_get_installed_version = mocker.patch(
            "getversions.main.get_installed_version",
            return_value=installed_version,
        )
        res = main(args)
        out, _ = capsys.readouterr()
        assert out == f"{exp_out}"
        assert res == exp_res
        if avail_versions is None:
            mock_get_avail_versions.assert_not_called()
        else:
            mock_get_avail_versions.assert_called_once_with(args[0])
        mock_get_installed_version.assert_called_once_with(args[0])
